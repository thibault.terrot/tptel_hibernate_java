package tptel.junit;



import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Test;

import tptel.dao.AbonnementDao;
import tptel.dao.ClientDao;
import tptel.dao.FactureDao;
import tptel.hibernate.HibernateUtil;
import tptel.modele.Abonnement;
import tptel.modele.Client;
import tptel.modele.Facture;


public class tptelHibernateUnitTest {

	@Test
	public void TestConnection() {
		System.out.println("TestConnection");
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
		assertNotNull(sessionFactory); //test ok si objet non null
		
		System.out.println("SessionFactory: "+sessionFactory);
	}
	@Test
	public void TestConnection2() {
		System.out.println("TestConnection2");
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		
		Session session = sessionFactory.openSession();
		assertNotNull(session); //test ok si objet non null
		
		System.out.println("Session: "+session);
	}
	@Test
    public void TestAbonnementQuery() {
		System.out.println("TestAbonnementQuery");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();        
        Session session = sessionFactory.openSession();

        //Hibernate 5
        CriteriaQuery<Abonnement> criteria = session.getCriteriaBuilder().createQuery(Abonnement.class);
        criteria.from(Abonnement.class);
        List<Abonnement> items = session.createQuery(criteria).getResultList();
        
        items.forEach(i->System.out.println(i));
    }
	
	@Test
    public void TestClientQuery() {
		System.out.println("TestClientQuery");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();        
        Session session = sessionFactory.openSession();

        //Hibernate 5
        CriteriaQuery<Client> criteria = session.getCriteriaBuilder().createQuery(Client.class);
        criteria.from(Client.class);
        List<Client> items = session.createQuery(criteria).getResultList();
        
        items.forEach(i->System.out.println(i));
    }
	
	@Test
    public void TestFactureQuery() {
		System.out.println("TestFactureQuery");
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();        
        Session session = sessionFactory.openSession();

        //Hibernate 5
        CriteriaQuery<Facture> criteria = session.getCriteriaBuilder().createQuery(Facture.class);
        criteria.from(Facture.class);
        List<Facture> items = session.createQuery(criteria).getResultList();
        
        items.forEach(i->System.out.println(i));
    }
	
	@Test
    public void TestClientDao() {
		System.out.println("TestClientDao");
        ClientDao dao = new ClientDao();
        List<Client> items = dao.list();
        System.out.println(dao);
        items.forEach(i->System.out.println(i));
        
        Client found = items.get(0);
        System.out.println("Found = "+found.getNom());
        
        dao.add(found);
    }
	
	@Test
    public void TestFactureDao() {
		System.out.println("TestFactureDao");
        FactureDao dao = new FactureDao();
        List<Facture> items = dao.list();
        System.out.println(dao);
        items.forEach(i->System.out.println(i));
       
        Facture found = items.get(0);
        System.out.println("Found = "+found.getAbonnement().getNomAbonnement() + " - " + found.getDateFact());
        
        dao.add(found);
    }
	
	
	@Test
    public void TestAbonnementDao() {
		System.out.println("TestAbonnementDao");
        AbonnementDao dao = new AbonnementDao();
        List<Abonnement> items = dao.list();
        System.out.println(dao);
        items.forEach(i->System.out.println(i));
       
        Abonnement found = items.get(0);
        System.out.println("Found = "+found.getNomAbonnement() + " - " + found.getClient().getNom());
        
        dao.add(found);
    }
	
	

}
