package tptel.dao;

 

import java.io.Serializable;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import tptel.hibernate.HibernateUtil;
import tptel.modele.Client;
import tptel.modele.Abonnement;
import tptel.modele.Facture;

 

public class Dao<T> implements IDao<T> {
    
	private static Session session = null;

 
	Class<T> entityClass;
	
	 public Dao(Class<T> entityClass) {
        if(session==null) {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
        }
        this.entityClass=entityClass;
    }
	    
    @Override
    public List<T> list() {
        //requete avec HQL : Hibernate Query Language
        
        //Hibernate 5
        Query<T> query = (Query<T>) session.createQuery("SELECT u FROM "+ this.entityClass.getName() +" u", this.entityClass);        
        return query.getResultList();

    }


    @Override
    public T get(Serializable id) {
        return session.get(this.entityClass, id);
    }

 
    @Override
    public void add(T item) {
    	Transaction transaction = session.beginTransaction();
        session.save(item);
        transaction.commit();
    }

 
    @Override
    public void update(T item) {
    	Transaction transaction = session.beginTransaction();
        session.update(item);
        transaction.commit();
    }

 
    @Override
    public void delete(Serializable id) {
    	T c = get(id);
    	if(c != null) delete(c);
    }


    @Override
    public void delete(T item) {
    	Transaction transaction = session.beginTransaction();
        session.delete(item);
        transaction.commit();
        
    }

}