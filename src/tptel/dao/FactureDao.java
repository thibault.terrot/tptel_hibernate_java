package tptel.dao;

 

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import tptel.hibernate.HibernateUtil;
import tptel.modele.Facture;
import tptel.modele.Facture;

 

public class FactureDao implements IDao<Facture> {
    
	private static Session session = null;

 

	 public FactureDao() {
        /**
         * Singleton
         */
        if(session==null) {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
        }
    }
	    
    @Override
    public List<Facture> list() {
        //requete avec HQL : Hibernate Query Language
        
        //Hibernate 5
        Query<Facture> query = (Query<Facture>) session.createQuery("SELECT u FROM Facture u", Facture.class);        
        return query.getResultList();

    }


    @Override
    public Facture get(Serializable id) {
        return session.get(Facture.class, id);
    }
 
    @Override
    public void add(Facture item) {
    	Transaction transaction = session.beginTransaction();
        session.save(item);
        transaction.commit();
    	
    }

 
    @Override
    public void update(Facture item) {
    	Transaction transaction = session.beginTransaction();
    	session.update(item);
    	transaction.commit();
        
    }

 
    @Override
    public void delete(Serializable id) {
    	Facture c = get(id);
    	if(c != null) delete(c);
    }


    @Override
    public void delete(Facture item) {
    	Transaction transaction = session.beginTransaction();
        session.delete(item);
        transaction.commit();
        
    }
}