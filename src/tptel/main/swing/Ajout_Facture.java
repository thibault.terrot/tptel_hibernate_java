package tptel.main.swing;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.swing.JFrame;

import tptel.dao.FactureDao;
import tptel.modele.Abonnement;
import tptel.modele.Facture;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

public class Ajout_Facture  extends JFrame {
	
	private Abonnement abo_courant;
	private JTextField text_new_fact_date;
	private JTextField text_new_fact_montant;
	private JLabel error_label;
	private MainSwing frame_from;
	
	public Ajout_Facture(MainSwing frame_from, Abonnement abo_courant){
		super("Ajouter une facture � un abonnement");
		
		this.frame_from = frame_from;
		this.abo_courant = abo_courant;
		
		getContentPane().setBackground(Color.GRAY);
		getContentPane().setForeground(Color.RED);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		
        error_label = new JLabel("");
        error_label.setForeground(Color.RED);
        error_label.setBounds(10, 11, 357, 25);
        getContentPane().add(error_label);
        
		JButton btnConfirm = new JButton("Ajouter");
		btnConfirm.setBounds(21, 327, 89, 23);
		getContentPane().add(btnConfirm);
		
		JButton btnCancel = new JButton("Annuler");
		btnCancel.setBounds(267, 327, 89, 23);
		getContentPane().add(btnCancel);
		Ajout_Facture current_frame = this;
		btnCancel.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    		current_frame.dispose();
	    	}
	    });
		
		JPanel panel = new JPanel();
		panel.setBounds(21, 82, 335, 218);
		getContentPane().add(panel);
		setBounds(300,300,400,400);
		
		
		panel.setBackground(Color.GRAY);
		panel.setBorder(new TitledBorder(null, "Ajouter une facure:", TitledBorder.LEADING, TitledBorder.TOP, null, Color.WHITE));
	    getContentPane().add(panel);
	    panel.setLayout(new GridLayout(0, 1, 0, 0));
	    
	    JLabel date_new_fact_label = new JLabel("Date:");
	    date_new_fact_label.setHorizontalAlignment(SwingConstants.CENTER);
	    date_new_fact_label.setForeground(Color.WHITE);
	    panel.add(date_new_fact_label);
	    
	    text_new_fact_date = new JTextField();
	    text_new_fact_date.setColumns(10);
	    panel.add(text_new_fact_date);
	    
	    JCheckBox reglee_new_fact = new JCheckBox("R\u00E9gl\u00E9e");
	    reglee_new_fact.setHorizontalAlignment(SwingConstants.CENTER);
	    reglee_new_fact.setForeground(Color.WHITE);
	    reglee_new_fact.setBackground(Color.GRAY);
	    panel.add(reglee_new_fact);
	    
	    
	    btnConfirm.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent arg0) {
	    		if(abo_courant != null) {
	    			Date new_date;
					try {
						FactureDao dao = new FactureDao();
						new_date = new SimpleDateFormat("dd-MM-yyyy").parse(text_new_fact_date.getText());
						Float montant = (float) 0.0;
						if(text_new_fact_montant.getText() != null) {
							if(text_new_fact_montant.getText().matches("[-+]?[0-9]*\\.?[0-9]+")) {  // Check seulement chiffre
								montant = Float.parseFloat(text_new_fact_montant.getText());
								Facture new_fact = new Facture(abo_courant, new_date , montant, reglee_new_fact.isSelected());
								dao.add(new_fact);
								
								// On recr�� la table pour afficher la nouvelle facture
								frame_from.clearTable();
								Set<Facture> liste_fact_courant = abo_courant.getFactures();
								liste_fact_courant.add(new_fact);
								abo_courant.setFactures(liste_fact_courant);
								for (Facture temp : abo_courant.getFactures()) {
		            	    		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		            	    		String strDate = dateFormat.format(temp.getDateFact());
		            	    		frame_from.addRow(strDate, String.valueOf(temp.getMontant()), temp.isReglee());
		            	    	}
								
								error_label.setForeground(Color.GREEN);
								error_label.setText("Facture ajout�e avec succ�s!");
							}
							else {
								error_label.setForeground(Color.RED);
								error_label.setText("Veuillez entrer un montant correct");
							}
						}
					} catch (ParseException e) {
						error_label.setForeground(Color.RED);
						error_label.setText("Veuillez entrer une date correcte ex: 20-02-2020");
						e.printStackTrace();
					}
	    		}else {
	    			error_label.setForeground(Color.RED);
					error_label.setText("Aucun abonnement s�l�ctionn�");
	    		}
	    	}
	    });
	    
	    JLabel montant_new_fact_label = new JLabel("Montant:");
	    montant_new_fact_label.setHorizontalAlignment(SwingConstants.CENTER);
	    montant_new_fact_label.setForeground(Color.WHITE);
	    panel.add(montant_new_fact_label);
	    
	    text_new_fact_montant = new JTextField();
	    text_new_fact_montant.setColumns(10);
	    panel.add(text_new_fact_montant);
	    
	    JLabel abo_courant_label = new JLabel("Abonnement n\u00B0" + abo_courant.getIdAbonnement());
	    abo_courant_label.setForeground(Color.WHITE);
	    abo_courant_label.setBounds(117, 47, 213, 14);
	    getContentPane().add(abo_courant_label);
	}
}
