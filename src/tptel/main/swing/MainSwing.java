package tptel.main.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;

import tptel.dao.AbonnementDao;
import tptel.dao.ClientDao;
import tptel.dao.FactureDao;
import tptel.main.swing.MainSwing;
import tptel.modele.Abonnement;
import tptel.modele.Client;
import tptel.modele.Facture;
import tptel.modele.Operateur;

import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import java.awt.SystemColor;
import javax.swing.border.BevelBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

public class MainSwing extends JFrame{
	
	private JPanel  pan_saisi_abo;
	private JPanel  pan_saisi_cli;
	private JTextField inputabo;
	private JTextField inputcli;
	private JTextField inputcliName;
	private JPanel pan_list_fact = new JPanel();
	private JPanel pan_list_abo = new JPanel();
	private JPanel pan_info_abo;
	private Abonnement abo_courant;
	private Client cli_courant;
	private Set<Facture> factures_courantes;
	private Set<Abonnement> abonnements_courants;
	private JLabel id_abo_label;
	private JLabel nom_abo_label;
	private JPanel pan_info_cli;
	private JLabel id_cli_label;
	private JLabel nom_cli_label;
	private JLabel date_abo_label;
	private JLabel solde_abo_label;
	private JLabel tel_cli_label;
	private JLabel email_cli_label;
	private JLabel ville_cli_label;
	private JLabel error_label;
	private JLabel total_fact;
	private JTable table_op;
	private JTable table_abo;

    private String[] entetes = {"Date", "Montant", "R�gl�e"};
    private String[] entetesabo = {"Id", "Nom", "Solde"};
	private DefaultTableModel tableModel = new DefaultTableModel(entetes, 0);
	private DefaultTableModel tableModelAbo = new DefaultTableModel(entetesabo, 0);
	
	private JButton btnNewButton;
	private JButton btnSearchCli;
	private JButton btnSearchCliByName;
	private JButton btnSearchAbo;
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		MainSwing tptel = new MainSwing();	
	}
	
	public MainSwing() throws ClassNotFoundException, SQLException {
		super("TPTelHibernate");
		
		// Main frame (abonnement)
		getContentPane().setBackground(Color.GRAY);
		getContentPane().setForeground(Color.RED);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        setBounds(100,100,800,800);
        
        createMenu();
        createPanSaisiAbo();
        createPanSaisiCli();
        createPanInfo();
        createPanListFact();     
        createPanListAbo();     
        createPanSaisiNewFact();

        setVisible(true);
		
	}

	public void createMenu() {
		JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        JMenu mnNewMenu = new JMenu("Abonnement");
        menuBar.add(mnNewMenu);
        
        JMenuItem mntmNewMenuItem = new JMenuItem("Information abonnement");
        mntmNewMenuItem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
                pan_saisi_abo.setVisible(true);
                pan_saisi_cli.setVisible(false);
                pan_info_abo.setVisible(true);
                pan_list_fact.setVisible(true);
                btnSearchCli.setVisible(false);
                btnSearchAbo.setVisible(true);
                btnSearchCliByName.setVisible(false);
                pan_list_abo.setVisible(false);
        	}
        });
        mnNewMenu.add(mntmNewMenuItem);
        
        JMenu mnNewMenu_1 = new JMenu("Client");
        menuBar.add(mnNewMenu_1);
        
        JMenuItem mntmNewMenuItem_2 = new JMenuItem("Information client");
        mntmNewMenuItem_2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
                pan_saisi_abo.setVisible(false);
                pan_saisi_cli.setVisible(true);
                pan_info_abo.setVisible(false);
                pan_list_fact.setVisible(false);
                btnSearchCli.setVisible(true);
                btnSearchAbo.setVisible(false);
                btnSearchCliByName.setVisible(true);
                pan_list_abo.setVisible(true);
        	}
        });
        mnNewMenu_1.add(mntmNewMenuItem_2);
	}
	
	
	public void createPanSaisiCli() {
		pan_saisi_cli = new JPanel();
		pan_saisi_cli.setBackground(Color.GRAY);
		pan_saisi_cli.setForeground(UIManager.getColor("Button.highlight"));
		pan_saisi_cli.setBounds(276, 56, 219, 117);
		pan_saisi_cli.setLayout(null);
		inputcli = new JTextField();
		inputcliName = new JTextField();
		inputcli.setBounds(10, 20, 195, 31);
		inputcliName.setBounds(10, 75, 195, 31);
        pan_saisi_cli.add(inputcli);
        pan_saisi_cli.add(inputcliName);
        pan_saisi_cli.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Num\u00E9ro de client:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
        getContentPane().add(pan_saisi_cli);
        pan_saisi_cli.setVisible(false);
        
        btnSearchCli = new JButton("Chercher");
        btnSearchCli.setForeground(Color.WHITE);
        btnSearchCli.setBackground(Color.DARK_GRAY);
        btnSearchCli.setBounds(505, 67, 103, 46);
        getContentPane().add(btnSearchCli);
        btnSearchCli.setVisible(false);
        
        btnSearchCliByName = new JButton("Chercher par nom");
        btnSearchCliByName.setForeground(Color.WHITE);
        btnSearchCliByName.setBackground(Color.DARK_GRAY);
        btnSearchCliByName.setBounds(505, 127, 147, 46);
        getContentPane().add(btnSearchCliByName);
        btnSearchCliByName.setVisible(false);

        btnSearchCli.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		if(inputcli.getText().isEmpty() || inputcli.getText().matches(".*[a-zA-Z].*")) {  // Check si lettre ou null
        			 error_label.setForeground(Color.RED);
        			 error_label.setText("Veuillez renseigner un num�ro de client correct");
        		}
        		else{
        			ClientDao dao = new ClientDao();
        			if(dao.get(Integer.parseInt(inputcli.getText())) != cli_courant) {
        				cli_courant = dao.get(Integer.parseInt(inputcli.getText()));
            			if(cli_courant != null) {
                			error_label.setText("");
                			id_cli_label.setText("Num�ro : " + cli_courant.getIdClient());
                			nom_cli_label.setText("Nom : " + cli_courant.getNom());
                			tel_cli_label.setText("T�l�phone : " + cli_courant.getTel());
                			email_cli_label.setText("Solde : " + cli_courant.getEmail());
                			ville_cli_label.setText("Ville : " + cli_courant.getVille());
                			abonnements_courants = cli_courant.getAbonnements();
                			
                			clearTableAbo();
                	    	for (Abonnement temp : abonnements_courants) {
                	    		addRowAbo(temp.getIdAbonnement(), temp.getNomAbonnement(), temp.getSolde());
                	    	}
            			}
            			else {
            				error_label.setForeground(Color.RED);
            				error_label.setText("Num�ro de client ind�finit");
            			}
        			}
        		}
        	}
        });
        
        btnSearchCliByName.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		
        		if(inputcliName.getText().isEmpty()) {  // Check si null
        			 error_label.setForeground(Color.RED);
        			 error_label.setText("Veuillez renseigner un nom de client correct");
        		}
        		else{
        			ClientDao dao = new ClientDao();
        			Operateur op = new Operateur();
        			if(op.chercheClientsParMotCles(inputcliName.getText()).size() != 0) {
        				if(op.chercheClientsParMotCles(inputcliName.getText()) != cli_courant) {
            				cli_courant = op.chercheClientsParMotCles(inputcliName.getText()).get(0);
            				
            				error_label.setForeground(Color.GREEN);
            				error_label.setText(op.chercheClientsParMotCles(inputcliName.getText()).size() + " clients trouv�s, si le client ne correspond pas veuillez taper son nom complet");
                			id_cli_label.setText("Num�ro : " + cli_courant.getIdClient());
                			nom_cli_label.setText("Nom : " + cli_courant.getNom());
                			tel_cli_label.setText("T�l�phone : " + cli_courant.getTel());
                			email_cli_label.setText("Solde : " + cli_courant.getEmail());
                			ville_cli_label.setText("Ville : " + cli_courant.getVille());
                			abonnements_courants = cli_courant.getAbonnements();
            			}
        			}
        			else {
        				error_label.setForeground(Color.RED);
        				error_label.setText("Aucun client trouv� avec ce nom");
        			}
        		}

        	}
        });
	}
	
	public void createPanSaisiAbo() {
		pan_saisi_abo = new JPanel();
        pan_saisi_abo.setBackground(Color.GRAY);
        pan_saisi_abo.setForeground(UIManager.getColor("Button.highlight"));
        pan_saisi_abo.setBounds(276, 56, 219, 63);
        pan_saisi_abo.setLayout(null);
        
        inputabo = new JTextField();
        inputabo.setBounds(10, 20, 195, 31);
        pan_saisi_abo.add(inputabo);
        pan_saisi_abo.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, new Color(255, 255, 255), new Color(160, 160, 160)), "Num\u00E9ro d'abonnement:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 255, 255)));
        getContentPane().add(pan_saisi_abo);
        
        btnSearchAbo = new JButton("Chercher");
        btnSearchAbo.setForeground(Color.WHITE);
        btnSearchAbo.setBackground(Color.DARK_GRAY);
        btnSearchAbo.setBounds(505, 67, 103, 46);
        getContentPane().add(btnSearchAbo);
        
        error_label = new JLabel("");
        error_label.setBackground(Color.WHITE);
        error_label.setForeground(Color.RED);
        error_label.setBounds(46, 11, 698, 25);
        getContentPane().add(error_label);
        
        btnSearchAbo.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		
        		if(inputabo.getText().isEmpty() || inputabo.getText().matches(".*[a-zA-Z].*")) {  // Check si lettre ou null
        			 error_label.setForeground(Color.RED);
        			 error_label.setText("Veuillez renseigner un num�ro d'abonnement correct");
        		}
        		else{
        			AbonnementDao dao = new AbonnementDao();
        			if(dao.get(Integer.parseInt(inputabo.getText())) != abo_courant) {
        				abo_courant = dao.get(Integer.parseInt(inputabo.getText()));
            			if(abo_courant != null) {
                			error_label.setText("");
                			cli_courant = abo_courant.getClient();
                			factures_courantes = abo_courant.getFactures();
                			
                			clearTable();
                	    	for (Facture temp : abo_courant.getFactures()) {
                	    		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                	    		String strDate = dateFormat.format(temp.getDateFact());
                	    		addRow(strDate, String.valueOf(temp.getMontant()), temp.isReglee());
                	    	}
                	        
                			id_abo_label.setText("Num�ro : " + abo_courant.getIdAbonnement());
                			nom_abo_label.setText("Nom : " + abo_courant.getNomAbonnement());
                			date_abo_label.setText("Date : " + abo_courant.getDateAbonnement());
                			solde_abo_label.setText("Solde : " + abo_courant.getSolde());
                			
                			id_cli_label.setText("Num�ro : " + cli_courant.getIdClient());
                			nom_cli_label.setText("Nom : " + cli_courant.getNom());
                			tel_cli_label.setText("T�l�phone : " + cli_courant.getTel());
                			email_cli_label.setText("Solde : " + cli_courant.getEmail());
                			ville_cli_label.setText("Ville : " + cli_courant.getVille());
                			Float total = (float) 0;
                			for(Facture fact : factures_courantes){
                				total += fact.getMontant();
                		    };
                			total_fact.setText("Total : "+total);
                			btnNewButton.setVisible(true);
            			}
            			else {
            				error_label.setForeground(Color.RED);
            				error_label.setText("Num�ro d'abonnement ind�finit");
            			}
        			}
        		}

        	}
        });
	}
	
	public void createPanInfo() {
        Border border_info_abo = BorderFactory.createTitledBorder("Abonnement:");

		pan_info_abo = new JPanel();
		pan_info_abo.setBackground(new Color(128, 128, 128));
		pan_info_abo.setForeground(UIManager.getColor("Button.darkShadow"));
        pan_info_abo.setBounds(65, 265, 643, 76);
        getContentPane().add(pan_info_abo);
        pan_info_abo.setLayout(new GridLayout(1, 2, 0, 0));
        id_abo_label = new JLabel("Num\u00E9ro :");
        id_abo_label.setForeground(Color.WHITE);
        pan_info_abo.add(id_abo_label);
        
        nom_abo_label = new JLabel("Nom :");
        nom_abo_label.setForeground(Color.WHITE);
        pan_info_abo.add(nom_abo_label);
        
        date_abo_label = new JLabel("Date :");
        date_abo_label.setForeground(Color.WHITE);
        pan_info_abo.add(date_abo_label);
        
        solde_abo_label = new JLabel("Solde :");
        solde_abo_label.setForeground(Color.WHITE);
        pan_info_abo.add(solde_abo_label);
        pan_info_abo.setBorder(new TitledBorder(null, "Abonnement: ", TitledBorder.LEADING, TitledBorder.TOP, null, Color.WHITE));
        
        btnNewButton = new JButton("Nouvelle facture");
        
        MainSwing current_frame = this;
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		new Ajout_Facture(current_frame, abo_courant).setVisible(true);
        	}
        });
        btnNewButton.setVisible(false); // On rend visible le boutton une fois qu'on a un abonnement courant
        pan_info_abo.add(btnNewButton);

        
        Border border_info_cli = BorderFactory.createTitledBorder("Client:");

        pan_info_cli = new JPanel();
        pan_info_cli.setForeground(UIManager.getColor("Button.highlight"));
        pan_info_cli.setBackground(new Color(128, 128, 128));
        pan_info_cli.setToolTipText("");
        pan_info_cli.setBounds(65, 178, 643, 76);
        getContentPane().add(pan_info_cli);
        pan_info_cli.setLayout(new GridLayout(0, 3, 0, 0));
        
        id_cli_label = new JLabel("Num\u00E9ro :");
        id_cli_label.setForeground(UIManager.getColor("Button.disabledShadow"));
        pan_info_cli.add(id_cli_label);
        
        nom_cli_label = new JLabel("Nom :");
        nom_cli_label.setForeground(UIManager.getColor("CheckBox.interiorBackground"));
        pan_info_cli.add(nom_cli_label);
        
        email_cli_label = new JLabel("Email :");
        email_cli_label.setForeground(UIManager.getColor("CheckBox.interiorBackground"));
        pan_info_cli.add(email_cli_label);
        
        tel_cli_label = new JLabel("T\u00E9l\u00E9phone :");
        tel_cli_label.setForeground(UIManager.getColor("CheckBox.interiorBackground"));
        pan_info_cli.add(tel_cli_label);
        
        ville_cli_label = new JLabel("Ville :");
        ville_cli_label.setForeground(UIManager.getColor("CheckBox.interiorBackground"));
        pan_info_cli.add(ville_cli_label);
        pan_info_cli.setBorder(new TitledBorder(null, "Client :", TitledBorder.LEADING, TitledBorder.TOP, null, Color.WHITE));

	}
	
	public void createPanListFact() {
		pan_list_fact.setBackground(new Color(128, 128, 128));
		pan_list_fact.setForeground(UIManager.getColor("Button.darkShadow"));
        pan_list_fact.setBorder(new TitledBorder(null, "Factures: ", TitledBorder.LEADING, TitledBorder.TOP, null, Color.WHITE));
        pan_list_fact.setBounds(116, 362, 552, 338);
        getContentPane().add(pan_list_fact);
        pan_list_fact.setLayout(new BorderLayout(0, 0));
        
        total_fact = new JLabel("Total :");
        total_fact.setFont(new Font("Tahoma", Font.BOLD, 11));
        total_fact.setBounds(618, 711, 126, 25);
        getContentPane().add(total_fact);
    	JScrollPane scrollTable = new JScrollPane();
        pan_list_fact.add(scrollTable, BorderLayout.NORTH);
        
        //Table
        table_op = new JTable(tableModel) {
        	public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
        	{
    		    Component c = super.prepareRenderer(renderer, row, column);
        		String desc = (String) getModel().getValueAt(row, 1);
    		    return c;
    		}
        };
        table_op.setRowSelectionAllowed(false);
        pan_list_fact.add(table_op, BorderLayout.CENTER);
        
        table_op.setFillsViewportHeight(true);
	}
	
	public void createPanListAbo(){
		pan_list_abo.setBackground(new Color(128, 128, 128));
		pan_list_abo.setForeground(UIManager.getColor("Button.darkShadow"));
		pan_list_abo.setBorder(new TitledBorder(null, "Abonnements: ", TitledBorder.LEADING, TitledBorder.TOP, null, Color.WHITE));
		pan_list_abo.setBounds(116, 265, 552, 435);
        getContentPane().add(pan_list_abo);
        pan_list_abo.setLayout(new BorderLayout(0, 0));
        
    	JScrollPane scrollTable = new JScrollPane();
    	pan_list_abo.add(scrollTable, BorderLayout.NORTH);
        
        //Table
        table_abo = new JTable(tableModelAbo) {
        	public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
        	{
    		    Component c = super.prepareRenderer(renderer, row, column);
        		String desc = (String) getModel().getValueAt(row, 1);
    		    return c;
    		}
        };
        table_abo.setRowSelectionAllowed(false);
        pan_list_abo.add(table_abo, BorderLayout.CENTER);
        
        table_abo.setFillsViewportHeight(true);
        pan_list_abo.setVisible(false);
	}
	
	public void createPanSaisiNewFact() {
        
	}
	public void clearTable()
	{
		tableModel.setRowCount(0);
	}
	
	public void clearTableAbo()
	{
		tableModelAbo.setRowCount(0);
	}
	
	public void addRow(String date, String montant, Boolean isreglee)
	{
		if(isreglee) {
			tableModel.addRow(new Object[]{date, montant, "R�gl�e"});
		}
		else {
			tableModel.addRow(new Object[]{date, montant, "Non R�gl�e"});
		}
	}
	
	public void addRowAbo(Integer id, String nom, Float solde)
	{
		tableModelAbo.addRow(new Object[]{id, nom, solde});
	}
}
