package tptel.modele;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import tptel.dao.AbonnementDao;
import tptel.dao.ClientDao;
import tptel.dao.FactureDao;

public class Operateur {

	public List<Client> chercheClientsParMotCles(String motcle) {

		ClientDao dao = new ClientDao();
		List<Client> allclients = dao.list();
		List<Client> clients = new ArrayList<Client>();
		
		allclients.forEach(i->{
	    	if(i.getNom().contains(motcle)) {
	    		clients.add(i);
	    	}
	    });
		
		return clients;
	}
	
	public Abonnement getAbonnement(int numAb) {
		AbonnementDao dao = new AbonnementDao();
		return dao.get(numAb);
	}
	
	public void addFacture(Abonnement abonnement, Date dateFact, float montant, boolean reglee) {
		FactureDao dao = new FactureDao();
		Facture fact = new Facture(abonnement, dateFact, montant, reglee);
		dao.add(fact);
	}
	
	public void addAbonnement(Client client, String nomAbonnement, Date dateAbonnement, float solde) {
		AbonnementDao dao = new AbonnementDao();
		Abonnement abo = new Abonnement(client, nomAbonnement, dateAbonnement, solde);
		dao.add(abo);
	}
	
	public void addClient(String nom, String email, String tel, String ville) {
		ClientDao dao = new ClientDao();
		Client cli = new Client(nom, email, tel, ville);
		dao.add(cli);
	}
}
