package tptel.dao;

 

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import tptel.hibernate.HibernateUtil;
import tptel.modele.Abonnement;
import tptel.modele.Abonnement;
import tptel.modele.Abonnement;

 

public class AbonnementDao implements IDao<Abonnement> {
    
	private static Session session = null;

 

	 public AbonnementDao() {
        /**
         * Singleton
         */
        if(session==null) {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
        }
    }
	    
    @Override
    public List<Abonnement> list() {
        //requete avec HQL : Hibernate Query Language
        
        //Hibernate 5
        Query<Abonnement> query = (Query<Abonnement>) session.createQuery("SELECT u FROM Abonnement u", Abonnement.class);        
        return query.getResultList();

    }


    @Override
    public Abonnement get(Serializable id) {
        return session.get(Abonnement.class, id);
    }
 
    @Override
    public void add(Abonnement item) {
    	Transaction transaction = session.beginTransaction();
        session.save(item);
        transaction.commit();
    	
    }

 
    @Override
    public void update(Abonnement item) {
    	Transaction transaction = session.beginTransaction();
    	session.update(item);
    	transaction.commit();
        
    }

 
    @Override
    public void delete(Serializable id) {
    	Abonnement c = get(id);
    	if(c != null) delete(c);
    }

	@Override
	public void delete(Abonnement item) {
    	Transaction transaction = session.beginTransaction();
        session.delete(item);
        transaction.commit();		
	}


}