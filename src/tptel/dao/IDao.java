package tptel.dao;

import java.io.Serializable;
import java.util.List;


public interface IDao<T> {

	/*
	 * CRUD
	 */
	List<T> list();
	T get(Serializable id);

	void add(T item);
	void update(T item);
	
	void delete(Serializable id);
	void delete(T item);	
}
