package tptel.dao;

 

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import tptel.hibernate.HibernateUtil;
import tptel.modele.Client;

 

public class ClientDao implements IDao<Client> {
    
	private static Session session = null;

 

	 public ClientDao() {
        /**
         * Singleton
         */
        if(session==null) {
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
        }
    }
	    
    @Override
    public List<Client> list() {
        //requete avec HQL : Hibernate Query Language
        
        //Hibernate 5
        Query<Client> query = (Query<Client>) session.createQuery("SELECT u FROM Client u", Client.class);        
        return query.getResultList();

    }


    @Override
    public Client get(Serializable id) {
        return session.get(Client.class, id);
    }
 
    @Override
    public void add(Client item) {
    	Transaction transaction = session.beginTransaction();
        session.save(item);
        transaction.commit();
    	
    }

 
    @Override
    public void update(Client item) {
    	Transaction transaction = session.beginTransaction();
    	session.update(item);
    	transaction.commit();
        
    }

 
    @Override
    public void delete(Serializable id) {
    	Client c = get(id);
    	if(c != null) delete(c);
    }


    @Override
    public void delete(Client item) {
    	Transaction transaction = session.beginTransaction();
        session.delete(item);
        transaction.commit();
        
    }
}