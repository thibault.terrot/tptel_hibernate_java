package tptel.junit;



import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.junit.Test;

import tptel.dao.AbonnementDao;
import tptel.dao.ClientDao;
import tptel.dao.FactureDao;
import tptel.hibernate.HibernateUtil;
import tptel.modele.Abonnement;
import tptel.modele.Client;
import tptel.modele.Facture;
import tptel.modele.Operateur;


public class tptelOperateurUnitTest {
	
	
	@Test
    public void TestChercheClient() {
		System.out.println("TestChercheClient");
		Operateur op = new Operateur();
		op.chercheClientsParMotCles("t").forEach(i->{
			System.out.println("Found with t: " + i.getNom() + " - " + i.getEmail() + " - " + i.getTel() + " - " + i.getVille());
	    });;
    }
	
	
	@Test
    public void TestOperateurNouveauClient() {
		System.out.println("TestOperateurNouveauClient");
		Operateur op = new Operateur();
		op.addClient("abcdefghijklmnopqrstuvxyz","t","t","t");
		
		op.chercheClientsParMotCles("abcdefghijklmnopqrstuvxyz").forEach(i->{
			System.out.println("Found: " + i.getNom() + " - " + i.getEmail() + " - " + i.getTel() + " - " + i.getVille());
	    });;
    }

	
}
